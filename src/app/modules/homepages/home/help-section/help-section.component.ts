import { Component, OnInit } from '@angular/core';
import { NpoService } from '../../../../core/_services';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';


@UntilDestroy()
@Component({
  selector: 'app-help-section',
  templateUrl: './help-section.component.html',
  styleUrls: ['./help-section.component.css']
})
export class HelpSectionComponent implements OnInit {

  public loading: boolean = false;
  npoCategories:any;
  search:any;


  constructor(
    private npoService: NpoService
  ) { }

  ngOnInit(): void {
    this.getNpoCategories();
  }

  public getNpoCategories():void{
    this.loading = true;
    this.npoService.getNpoCategories().pipe(untilDestroyed(this)).subscribe(response => {
      
      this.loading = false;
      this.npoCategories = response.data;
    }, error => {
      this.loading = false;
    })
  }

}
