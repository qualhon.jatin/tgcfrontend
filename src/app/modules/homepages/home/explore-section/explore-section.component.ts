import { Component, OnInit } from '@angular/core';
//import core services
import { PrizesService } from '../../../../core/_services';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'; 


@UntilDestroy()
@Component({
  selector: 'app-explore-section',
  templateUrl: './explore-section.component.html',
  styleUrls: ['./explore-section.component.css']
})
export class ExploreSectionComponent implements OnInit {

  public loading: boolean = false;
  allPrizes:any;

  constructor(
    private prizeService: PrizesService
  ) { }

  ngOnInit(): void {
    this.getPrizes();
  }

  public getPrizes():void{
    this.loading = true;
    this.prizeService.getPrizes().pipe(untilDestroyed(this)).subscribe(response => {
      this.loading = false;
      this.allPrizes = response.data;
    }, error => {
      this.loading = false;
    })
  }

}
