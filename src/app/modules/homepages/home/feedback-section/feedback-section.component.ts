import { Component, OnInit } from '@angular/core';

import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-feedback-section',
  templateUrl: './feedback-section.component.html',
  styleUrls: ['./feedback-section.component.css']
})
export class FeedbackSectionComponent implements OnInit {

  customOptions: OwlOptions = {
    loop: true,
    autoplay: true,
    margin: 0,
    nav: false,
    dotsData: false,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  }
  

  constructor() { }

  ngOnInit(): void {
  }

}
