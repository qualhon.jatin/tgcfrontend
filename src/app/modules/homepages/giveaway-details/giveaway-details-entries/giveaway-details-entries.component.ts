import { Component, OnInit, Input } from '@angular/core';
import { CommonUtilsService } from 'src/app/core/_services/common-utils.service';
import { UsersService } from '../../../../core/_services/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-giveaway-details-entries',
  templateUrl: './giveaway-details-entries.component.html',
  styleUrls: ['./giveaway-details-entries.component.css'],
})
export class GiveawayDetailsEntriesComponent implements OnInit {
  // tslint:disable-next-line: variable-name
  show_more_entries: boolean;
  // tslint:disable-next-line: variable-name
  show_more_entries_button: boolean;
  prizeEntries: any;
  cartItems = [];
  savedCartItems: any[];
  newCartItem = true;

  constructor(
    private commonUtilsService: CommonUtilsService,
    private userService: UsersService,
    private router: Router
  ) {}

  @Input() prizesDetail;

  ngOnInit(): void {
    this.show_more_entries = false;
    this.show_more_entries_button = true;
    setTimeout(() => {
      this.prizeEntries = JSON.parse(this.prizesDetail.prize_entry_values);
    }, 1000);
  }

  public give_more_entries() {
    this.show_more_entries = true;
    this.show_more_entries_button = false;
  }

  public addToCart(entryInfo, entryPrizeDetail) {

    const newItem = {
      giveaway_id: entryPrizeDetail.id,
      giveaway_title: entryPrizeDetail.prize_title,
      entry_amount: entryInfo.entry_value,
      total_entries: entryInfo.entry_value * 10,
      giveaway_image: entryPrizeDetail.prize_sponser_image,
      quantity: 1,
    };

    if (localStorage.getItem('isLoggedIn')) {
      this.userService.addToCart(newItem).subscribe(response => {
        if (response) {
          this.commonUtilsService.onSuccess('Item added to cart');
        } else {
        }
      }, error => {
      });
    } else {
      this.cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
      for (const item in this.cartItems) {
        if (
          this.cartItems[item].giveaway_id === newItem.giveaway_id &&
          this.cartItems[item].entry_amount === newItem.entry_amount
        ) {
          this.cartItems[item].quantity++;
          localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
          this.router.navigate(['checkout', 'category-list']);
          return;
        }
      }
      this.cartItems.push(newItem);
      localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
      this.commonUtilsService.onSuccess('Item added to cart');
    }
    this.router.navigate(['checkout', 'category-list']);
  }
}
