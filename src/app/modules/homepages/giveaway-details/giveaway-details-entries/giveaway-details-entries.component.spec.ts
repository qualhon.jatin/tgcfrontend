import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiveawayDetailsEntriesComponent } from './giveaway-details-entries.component';

describe('GiveawayDetailsEntriesComponent', () => {
  let component: GiveawayDetailsEntriesComponent;
  let fixture: ComponentFixture<GiveawayDetailsEntriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiveawayDetailsEntriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiveawayDetailsEntriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
