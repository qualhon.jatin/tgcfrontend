import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiveawayDetailsDescriptionComponent } from './giveaway-details-description.component';

describe('GiveawayDetailsDescriptionComponent', () => {
  let component: GiveawayDetailsDescriptionComponent;
  let fixture: ComponentFixture<GiveawayDetailsDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiveawayDetailsDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiveawayDetailsDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
