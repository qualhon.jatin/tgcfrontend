import { Component, OnInit,Input } from '@angular/core';

import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-giveaway-details-description',
  templateUrl: './giveaway-details-description.component.html',
  styleUrls: ['./giveaway-details-description.component.css']
})
export class GiveawayDetailsDescriptionComponent implements OnInit {

  customOptions: OwlOptions = {
    loop: true,
    autoplay: true,
    margin: 0,
    nav: true,
    navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
    dotsData: false,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  }

  @Input() prizesDetail;
  prize_end_date: any;
  prize_end_date_countdown_timer : any;
  prize_day_left : any;
  prize_second_left : any;
  firstDate : any;
  secondDate : any;

  
  

  

  

  constructor() { }

  ngOnInit(): void {
    

    setTimeout(() => {

      this.getCountdownTime();
     
    }, 1000);
    this.enter_to_win();
    

  }

  public  getCountdownTime(){
    this.prize_end_date = this.prizesDetail.prize_end_date;
      var prize_end_date_second = Date.parse(this.prize_end_date);
      this.prize_end_date = prize_end_date_second/1000;

      const oneDay = 24 * 60 * 60 * 1000;
      this.firstDate = new Date();
      this.secondDate = new Date(this.prizesDetail.prize_end_date);
      this.prize_day_left = Math.round(Math.abs((this.firstDate - this.secondDate) / oneDay));

      var midnight = new Date();
      midnight.setHours(24,0,0,0);

      this.prize_second_left =  Math.round(midnight.getTime()/1000) - Math.round(new Date().getTime()/1000) ;

      this.prize_end_date_countdown_timer = {'leftTime':this.prize_second_left, 'format' : 'HH:mm:ss'};

  }
  public enter_to_win(){
    document.getElementById("entry-level-sec").scrollIntoView()
  }

}
