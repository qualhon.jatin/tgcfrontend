import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GiveawayDetailsRoutingModule } from './giveaway-details-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    GiveawayDetailsRoutingModule
  ]
})
export class GiveawayDetailsModule { }
