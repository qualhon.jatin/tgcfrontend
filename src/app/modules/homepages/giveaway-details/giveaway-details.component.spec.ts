import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiveawayDetailsComponent } from './giveaway-details.component';

describe('GiveawayDetailsComponent', () => {
  let component: GiveawayDetailsComponent;
  let fixture: ComponentFixture<GiveawayDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiveawayDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiveawayDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
