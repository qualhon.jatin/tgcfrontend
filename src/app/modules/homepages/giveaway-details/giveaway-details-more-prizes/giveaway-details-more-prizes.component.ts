import { Component, OnInit, Input } from '@angular/core';
// import core services
import { PrizesService } from '../../../../core/_services';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-giveaway-details-more-prizes',
  templateUrl: './giveaway-details-more-prizes.component.html',
  styleUrls: ['./giveaway-details-more-prizes.component.css'],
})
export class GiveawayDetailsMorePrizesComponent implements OnInit {
  public loading = false;
  allPrizes: any;

  @Input() prizesDetail;

  constructor(private prizeService: PrizesService) {}

  ngOnInit(): void {
    setTimeout(() => {}, 1000);
    this.getPrizes();
  }

  public getPrizes(): void {
    this.loading = true;
    this.prizeService
      .relatedPrizes(this.prizesDetail.id)
      .pipe(untilDestroyed(this))
      .subscribe(
        (response) => {
          this.loading = false;
          this.allPrizes = response.data;
        },
        (error) => {
          this.loading = false;
        }
      );
  }
}
