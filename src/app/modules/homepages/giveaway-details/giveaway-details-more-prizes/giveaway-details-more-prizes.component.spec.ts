import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiveawayDetailsMorePrizesComponent } from './giveaway-details-more-prizes.component';

describe('GiveawayDetailsMorePrizesComponent', () => {
  let component: GiveawayDetailsMorePrizesComponent;
  let fixture: ComponentFixture<GiveawayDetailsMorePrizesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiveawayDetailsMorePrizesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiveawayDetailsMorePrizesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
