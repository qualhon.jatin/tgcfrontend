import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PrizesService } from '../../../core/_services';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BehaviorSubject } from 'rxjs';

@UntilDestroy()
@Component({
  selector: 'app-giveaway-details',
  templateUrl: './giveaway-details.component.html',
  styleUrls: ['./giveaway-details.component.css'],
})
export class GiveawayDetailsComponent implements OnInit {
  prize_id: string;
  public loading = false;
  public prizesDetail: any;
  private profileObs = new BehaviorSubject(null);

  constructor(
    private activatedRoute: ActivatedRoute,
    private prizeService: PrizesService
  ) {
    this.prize_id = this.activatedRoute.snapshot.params.id;
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response) => {
      if (response.giveawayDetail) {
        this.prizesDetail = response.giveawayDetail.data;
      }
    });
    // this.getPrizeDetail();
  }

  /*
    Function name : getPrizeDetail
    Function Purpose : Get prize detail by id
    Created by : Jatin
    Created on : 5 Oct 2020
  */

  // public getPrizeDetail(): void {
  //   this.loading = true;
  //   this.prizeService
  //     .prizeDetail(this.prize_id)
  //     .pipe(untilDestroyed(this))
  //     .subscribe(
  //       (response) => {
  //         this.loading = false;
  //         this.prizesDetail = response.data;
  //         console.log(this.prizesDetail);
  //         this.profileObs.next(response.data);
  //       },
  //       (error) => {
  //         this.loading = false;
  //       }
  //     );
  // }
  public enter_to_win() {
    alert('enter_to_win_2');
  }
}
