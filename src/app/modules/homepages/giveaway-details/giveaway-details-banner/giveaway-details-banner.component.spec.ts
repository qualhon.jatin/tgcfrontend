import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiveawayDetailsBannerComponent } from './giveaway-details-banner.component';

describe('GiveawayDetailsBannerComponent', () => {
  let component: GiveawayDetailsBannerComponent;
  let fixture: ComponentFixture<GiveawayDetailsBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiveawayDetailsBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiveawayDetailsBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
