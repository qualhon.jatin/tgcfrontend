import { Component, OnInit } from '@angular/core';
import { PrizesService } from '../../../core/_services';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-giveaways',
  templateUrl: './giveaways.component.html',
  styleUrls: ['./giveaways.component.css']
})
export class GiveawaysComponent implements OnInit {

  public loading: boolean = false;
  allPrizes:any

  constructor(
    private prizeService: PrizesService
  ) { }

  ngOnInit(): void {
    this.getPrizes();
  }

  public getPrizes():void{
    this.loading = true;
    this.prizeService.getPrizes().pipe(untilDestroyed(this)).subscribe(response => {
      this.loading = false;
      this.allPrizes = response.data;
    }, error => {
      this.loading = false;
    })
  }
}
