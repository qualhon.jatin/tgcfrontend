import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component'; 
import { ProfileComponent } from './profile/profile.component';


// Load Common Components
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';

import { SharedModule } from '../../core/shared/shared.module'; // Shared Module
import { MyPurchasesReolver } from 'src/app/core/resolvers/my-purchases.resolver';



@NgModule({
  declarations: [UsersComponent, LoginComponent, RegisterComponent, ForgotPasswordComponent, ResetPasswordComponent, ProfileComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.wanderingCubes,
      backdropBackgroundColour: '#002249',
      backdropBorderRadius: '4px',
      primaryColour: '#002249',
      secondaryColour: '#002249',
      tertiaryColour: '#002249'
    }),
  ],
  providers: [
    MyPurchasesReolver
  ]
})
export class UsersModule { }
