import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ViewEncapsulation,
  NgZone,
} from '@angular/core';
import { Location } from '@angular/common';
import {
  AbstractControl,
  FormBuilder,
  FormArray,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { of, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

// import Toaster Notification
import { ToastrService } from 'ngx-toastr';

// import Lodash
import * as _ from 'lodash';

// import environment
import { environment } from '../../../../environments/environment';

// import core services
import { UsersService, CommonUtilsService } from '../../../core/_services';

// import Custom Validator
import { CustomValidator } from '../../../core/helpers/custom-validator';

@UntilDestroy()
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginSubmitted = false;
  returnToUrl: string;
  public loading = false;

  constructor(
    private formBuilder: FormBuilder,
    private commonUtilsService: CommonUtilsService,
    private userAuthService: UsersService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      if (params.hasOwnProperty('returnToUrl')) {
        this.returnToUrl = params.returnToUrl;
      }
    });
    // if User Logged In then redirect to Dashboard Page
    this.userAuthService.checkLoginAndRedirect();
    this.buildLoginForm();
  }

  /**
   * Initialize End User Login Fields.
   */
  private buildLoginForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  /**
   * validate login form .
   * @return json string
   */
  validateLoginForm(): void {
    this.loginSubmitted = true;
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.userAuthService
      .userLogin(this.loginForm.value)
      .pipe(untilDestroyed(this))
      .subscribe(
        // case success
        (res) => {
          this.loading = false;
          this.commonUtilsService.onSuccess(res.body.response);
          localStorage.setItem('x-auth-token', res.headers.get('x-auth-token'));
          localStorage.setItem('account_type', res.body.accountType);
          localStorage.setItem('isLoggedIn', JSON.stringify(true));
          this.userAuthService.isLoggedIn(true, res.accountType); // trigger loggedin observable
          // this.router.navigate(['/user/dashboard']);

          this.addItemsToCart();
          if (this.returnToUrl) {
            this.router.navigateByUrl(this.returnToUrl);
          } else {
            this.router.navigate(['/user/profile']);
          }
          // case error
        },
        (error) => {
          this.loading = false;
          this.commonUtilsService.onError(
            error.response
              ? error.response
              : environment.MESSAGES.VALIDATION_ERROR
          );
        }
      );
  }

  addItemsToCart = () => {
    const cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
    if (cartItems.length) {
      cartItems.forEach(element => {
        this.userAuthService.addToCart(element).subscribe(response => {
          if (response) {
          }
        }, error => {
        });
      });
      localStorage.removeItem('cartItems');
    }
  }

  // This method must be present, even if empty.
  // tslint:disable-next-line: use-lifecycle-interface
  ngOnDestroy() {
    // To protect you, we'll throw an error if it doesn't exist.
  }
}
