import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ViewEncapsulation,
  NgZone,
} from '@angular/core';
import { Location } from '@angular/common';
import {
  AbstractControl,
  FormBuilder,
  FormArray,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { of, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
// import Lodash
import * as _ from 'lodash';

// import environment
import { environment } from '../../../../environments/environment';

// import core services
import { UsersService, CommonUtilsService } from '../../../core/_services';

// import Custom Validator
import { CustomValidator } from '../../../core/helpers/custom-validator';

@UntilDestroy()
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  isSubmitted = false;
  userProfileForm: FormGroup;
  purchases: any[];
  activeTab: string;
  public loading = false;

  constructor(
    private zone: NgZone,
    private formBuilder: FormBuilder,
    private commonUtilsService: CommonUtilsService,
    private userAuthService: UsersService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.initUserProfileForm(); // initilize user profile form
    this.getUserProfileData();

    this.activatedRoute.data.subscribe((response) => {
      if (response.purchases.data) {
        this.purchases = response.purchases.data;
        console.log(this.purchases);
      }
    });
    this.activatedRoute.queryParams.subscribe(data => {
      if (data.tabName) {
        this.activeTab = data?.tabName;
      }
    });
  }

  private initUserProfileForm(): void {
    this.userProfileForm = this.formBuilder.group(
      {
        first_name: ['', Validators.required],
        last_name: ['', Validators.required],
        name: [''],
        email: ['', [Validators.email, Validators.required]],
        password: [
          '',
          Validators.compose([
            Validators.minLength(8),
            Validators.maxLength(50),
            // check whether the entered password has a number
            CustomValidator.patternValidator(/\d/, {
              hasNumber: true,
            }),
            // check whether the entered password has upper case letter
            CustomValidator.patternValidator(/[A-Z]/, {
              hasCapitalCase: true,
            }),
            // check whether the entered password has a lower case letter
            CustomValidator.patternValidator(/[a-z]/, {
              hasSmallCase: true,
            }),
            // check whether the entered password has a special character
            CustomValidator.patternValidator(
              /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
              {
                hasSpecialCharacters: true,
              }
            ),
          ]),
        ],
        confirm_password: [''],
      },
      {
        validator: CustomValidator.MustMatch('password', 'confirm_password'),
      }
    );
  }

  /**
   * get Profile Data
   */
  private getUserProfileData(): void {
    this.loading = true;
    this.userAuthService
      .getUserProfile()
      .pipe(untilDestroyed(this))
      .subscribe(
        (response) => {
          this.patchProfile(response);
          this.loading = false;
        },
        (error) => {
          this.loading = false;
        }
      );
  }

  /**
   * Update Values in All Fields
   */
  private patchProfile(response: any): void {
    const profile = response.user_details;
    this.userProfileForm.patchValue({
      first_name: profile.first_name,
      last_name: profile.last_name,
      email: profile.email,
    });
  }

  /**
   * Submit Profile Update
   */
  public submitProfile(): void {
    this.isSubmitted = true;
    if (this.userProfileForm.invalid) {
      return;
    }

    this.loading = true;

    const body = this.userProfileForm.value;
    // tslint:disable-next-line: no-string-literal
    body['name'] =
      // tslint:disable-next-line: no-string-literal
      this.userProfileForm.controls['first_name'].value +
      ' ' +
      // tslint:disable-next-line: no-string-literal
      this.userProfileForm.controls['last_name'].value;

    this.userAuthService
      .updateProfile(body)
      .pipe(untilDestroyed(this))
      .subscribe(
        (response) => {
          this.isSubmitted = false;
          this.loading = false;
          this.userAuthService.isProfileUpdated(true);
          this.commonUtilsService.onSuccess(
            environment.MESSAGES.PROFILE_UPDATE
          );
        },
        (error) => {
          this.isSubmitted = false;
          this.loading = false;
          this.commonUtilsService.onError(error.response);
        }
      );
  }
  onChangeTab = (tabName: string) => {
    this.router.navigate(['user', 'profile'], {
      queryParams: {
        tabName
      }
    });
  }
}
