import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { AbstractControl, FormBuilder, FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';
import { of, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

//import Toaster Notification
import { ToastrService } from 'ngx-toastr';

//import Lodash
import * as _ from 'lodash';

// import environment
import { environment } from '../../../../environments/environment';

//import core services
import { UsersService, CommonUtilsService } from '../../../core/_services';

//import Custom Validator
import { CustomValidator } from '../../../core/helpers/custom-validator';

@UntilDestroy()
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  forgotPasswordForm: FormGroup;
  forgotPasswordSubmitted = false;
  public loading = false;

  constructor(private formBuilder: FormBuilder, private commonUtilsService: CommonUtilsService, private userAuthService: UsersService, private router: Router) { }

  /**
 * Initialize Forgot Password Fields.
 */
  private buildForgotPasswordForm() {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  /**
   * Submit Forgot Password.
   * return object array 
   */
  validateForgotPasswordForm(): void {

    this.forgotPasswordSubmitted = true;
    if (this.forgotPasswordForm.invalid) {
      return;
    }
    this.loading = true;

    this.userAuthService.userForgotPassword(this.forgotPasswordForm.value).pipe(untilDestroyed(this)).subscribe(
      //case success
      (res) => {
        this.loading = false;
        this.forgotPasswordSubmitted = false;
        this.commonUtilsService.onSuccess(res.response);
        this.router.navigate(['/user/login']);
        //case error 
      }, error => {
        this.loading = false;
        this.forgotPasswordSubmitted = false;
        this.commonUtilsService.onError(error.response);
      });
  }

  ngOnInit(): void {
    // if User Logged In then redirect to Dashboard Page
    this.userAuthService.checkLoginAndRedirect();
    this.buildForgotPasswordForm();
  }

  // This method must be present, even if empty.
  ngOnDestroy() {
    // To protect you, we'll throw an error if it doesn't exist.
  }

}
