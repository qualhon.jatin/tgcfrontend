import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component'; 
import { ProfileComponent } from './profile/profile.component';

import { UserAuthGuardService } from '../../core/guards/user-auth-guard.service';
import { LoginGuardService } from 'src/app/core/guards/login.guard';
import { MyPurchasesReolver } from 'src/app/core/resolvers/my-purchases.resolver';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'User Login' },
    canActivate: [LoginGuardService]
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: { title: 'User Signup' },
    canActivate: [LoginGuardService]
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,
    data: { title: 'Forgot Password' }
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent,
    data: { title: 'Reset Password' }
  },
  {
    path: 'profile',
    component: ProfileComponent,
    data: { title: 'Profile' },
    canActivate: [UserAuthGuardService],
    resolve: {
      purchases: MyPurchasesReolver
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
