import { Component, OnInit } from '@angular/core';
import { NpoService } from '../../../core/_services';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ActivatedRoute, Router } from '@angular/router';

@UntilDestroy()
@Component({
  selector: 'app-charity-list',
  templateUrl: './charity-list.component.html',
  styleUrls: ['./charity-list.component.css']
})
export class CharityListComponent implements OnInit {

  public loading = false;
  npoListAll: any;
  npoList: any[];
  npoCategories: any;
  search: any;
  // tslint:disable-next-line: variable-name
  category_id: any;
  mySelect = '11';
  selectedCategoryInfo: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private npoService: NpoService,
    private router: Router
  ) {
    this.category_id = this.activatedRoute.snapshot.params.id;
  }

  ngOnInit(): void {
    this.getNpoCategories();
    this.getNPOListByCategory(this.category_id);
  }

  public getNpoCategories(): void {
    this.loading = true;
    this.npoService.getNpoCategories().pipe(untilDestroyed(this)).subscribe(response => {
      this.loading = false;
      this.npoCategories = response.data;

      // tslint:disable-next-line: triple-equals
      this.selectedCategoryInfo =  this.npoCategories.find(x => x.id == this.category_id);
    }, error => {
      this.loading = false;
    });
  }

  // tslint:disable-next-line: variable-name
  public getNPOListByCategory(category_id): void {
    this.loading = true;
    this.npoService.getNPOListByCategory(category_id).pipe(untilDestroyed(this)).subscribe(response => {
      this.loading = false;
      this.npoListAll = response.data;
      this.npoList =  this.npoListAll;
      // this.npoList =  this.npoListAll.filter(x => x.organization_name == this.search);

    }, error => {
      this.loading = false;
    });
  }
  public changeCategory(){
    this.getNPOListByCategory(this.category_id);
    // tslint:disable-next-line: triple-equals
    this.selectedCategoryInfo =  this.npoCategories.find(x => x.id == this.category_id);
  }
  public searchCharity(){
    let regexSearch = new RegExp(this.search, 'gi');
    // tslint:disable-next-line: triple-equals
    this.npoList =  this.npoListAll.filter(x => x.organization_name.match(regexSearch));
  }

  onRedirectCartItems = (charityId: number) => {
    this.router.navigate(['checkout', 'cart-items', charityId]);
  }

}
