import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersService } from '../../../core/_services/users.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-cart-items',
  templateUrl: './cart-items.component.html',
  styleUrls: ['./cart-items.component.css']
})
export class CartItemsComponent implements OnInit {
  cartItemsSubscription: Subscription;
  charityId: number;
  cartItems: any[] = [];
  isLoggedIn: boolean;
  totalAmount = 0;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private userService: UsersService) { }

  ngOnInit(): void {
    this.charityId = this.activatedRoute.snapshot.params.id;
    this.cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
    // this.activatedRoute.data.subscribe(
    //   response => {
    //     this.cartItems = response?.cartItems?.data;
    //   }
    // );
    if (localStorage.getItem('isLoggedIn')) {
      this.isLoggedIn = true;
      this.cartItemsSubscription = this.userService.getCartItems().subscribe(items => {
        this.cartItems = [
          ...items.data
        ];
        this.calculateTotalAmount();
      });
    }
    this.calculateTotalAmount();
  }

  calculateTotalAmount = () => {
    this.totalAmount = 0;
    if (this.cartItems.length) {
      this.cartItems.forEach((item) => {
        this.totalAmount += (parseInt(item.entry_amount, 10) * parseInt(item.quantity, 10));
      });
    }
  }

  onRedirectCheckout = () => {
    this.router.navigate(['checkout', 'payment-detail', this.charityId]);
  }

  onRemoveFromCart = (itemIndex: number, cartId: number) => {
    if (this.isLoggedIn) {
      this.userService.removeCartItem(this.cartItems[itemIndex]).subscribe(response => {
        if (response) {
          this.cartItems.splice(itemIndex, 1);
        }
      });
    } else if (!cartId) {
      this.cartItems.splice(itemIndex, 1);
      localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
    }
  }

  onDecreaseQuantity = (index: number, cartId: number) => {
    if (parseInt(this.cartItems[index].quantity, 10) - 1 === 0) {
      return;
    }
    this.cartItems[index].quantity = parseInt(this.cartItems[index].quantity, 10) - 1;
    if (this.isLoggedIn) {
      this.updateCartItem(index);
    } else if (!cartId) {
      localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
    }
    this.calculateTotalAmount();
  }
  onIncreaseQuantity = (index: number, cartId: number) => {
    this.cartItems[index].quantity = parseInt(this.cartItems[index].quantity, 10) + 1;
    if (this.isLoggedIn) {
      this.updateCartItem(index);
    } else if (!cartId) {
      localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
    }
    this.calculateTotalAmount();
  }

  onChangeQuantity = (index: number, cartId: number, quantity: any, ) => {
    if (quantity <= 0) {
      return;
    }
    this.cartItems[index].quantity = parseInt(quantity, 10);
    if (this.isLoggedIn) {
      this.updateCartItem(index);
    } else if (!cartId) {
      localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
    }
    this.calculateTotalAmount();
  }

  updateCartItem = (index: number) => {
    this.userService.updateCartItem(this.cartItems[index]).subscribe(response => {
      if (response) {
      }
    });
  }

  ngOnDestroy = () => {
    if (this.cartItemsSubscription) {
      this.cartItemsSubscription.unsubscribe();
    }
  }
}
