import { Component, OnInit } from '@angular/core';
import { NpoService } from '../../../core/_services';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Router } from '@angular/router';


@UntilDestroy()
@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  public loading = false;
  selectedCategoryId: number;
  npoCategories: any;

  constructor(
    private npoService: NpoService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getNpoCategories();
  }

  public getNpoCategories(): void {
    this.loading = true;
    this.npoService.getNpoCategories().pipe(untilDestroyed(this)).subscribe(response => {
      this.loading = false;
      this.npoCategories = response.data;
    }, error => {
      this.loading = false;
    });
  }

  onSelectBlock = (categoryId) => {
    this.selectedCategoryId = categoryId;
  }
  redirectToNPOPage = () => {
    this.router.navigate(['checkout', 'charity-list', this.selectedCategoryId]);
  }

}
