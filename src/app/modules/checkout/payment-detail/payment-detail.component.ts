import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
  AfterViewInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../../../core/_services/users.service';
import { Subscription } from 'rxjs/Subscription';
import { NgForm } from '@angular/forms';
import { AngularStripeService } from '@fireflysemantics/angular-stripe-service';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.component.html',
  styleUrls: ['./payment-detail.component.css'],
})
export class PaymentDetailComponent
  implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('cardNumber', { static: false }) cardNumber: ElementRef;
  @ViewChild('expiryDate', { static: false }) expiryDate: ElementRef;
  @ViewChild('securityCode', { static: false }) securityCode: ElementRef;
  stripe;
  loading = false;
  confirmation;

  card: any;
  cardExpiry: any;
  cardCvc: any;

  cardNumberError: string;
  expiryDateError: string;
  securityCodeError: string;

  cartItemsSubscription: Subscription;
  charityId: number;
  cartItems: any[] = [];
  isLoggedIn: boolean;
  totalAmount = 0;

  couponError: string;
  couponVerified = false;
  couponData: number;
  // tslint:disable-next-line: variable-name
  coupon_code: string;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private stripeService: AngularStripeService,
    private userService: UsersService
  ) {}

  ngOnInit(): void {
    this.charityId = this.activatedRoute.snapshot.params.id;
    this.activatedRoute.data.subscribe((response) => {
      this.cartItems = response.cartItems.data;
    });
    this.calculateTotalAmount();
  }

  ngAfterViewInit() {
    this.stripeService
      .setPublishableKey(
        'pk_test_51H7wKYHIHAAMdDwa5713QNLj0RW5LYPtXddN80zpioEYKpC7P3onl7WX6HWKelYFEeNs6xrQZV6dH9V3ZzKVQ6JX00GvYWcy01'
      )
      .then((stripe) => {
        this.stripe = stripe;
        const elements = stripe.elements();

        this.card = elements.create('cardNumber', {
          placeholder: 'Card Number',
        });
        this.card.addEventListener('change', (event) => {
          this.cardNumberError = null;
          if (event.error) {
            this.cardNumberError = event.error.message;
          }
        });
        this.card.mount(this.cardNumber.nativeElement);
        this.cardExpiry = elements.create('cardExpiry', {
          placeholder: 'Card Expiry',
          class: 'form-control',
        });
        this.cardExpiry.addEventListener('change', (event) => {
          this.expiryDateError = null;
          if (event.error) {
            this.expiryDateError = event.error.message;
          }
        });
        this.cardExpiry.mount(this.expiryDate.nativeElement);
        this.cardCvc = elements.create('cardCvc', {
          placeholder: 'Card CVV',
          class: 'form-control',
        });
        this.cardCvc.addEventListener('change', (event) => {
          this.securityCodeError = null;
          if (event.error) {
            this.securityCodeError = event.error.message;
          }
        });
        this.cardCvc.mount(this.securityCode.nativeElement);
        // this.card.addEventListener('change', this.cardHandler);
      });
  }

  calculateTotalAmount = () => {
    if (this.cartItems.length) {
      this.cartItems.forEach((item) => {
        this.totalAmount +=
          parseInt(item.entry_amount, 10) * parseInt(item.quantity, 10);
      });
    }
  }

  ngOnDestroy = () => {
    if (this.cartItemsSubscription) {
      this.cartItemsSubscription.unsubscribe();
    }
    this.card.destroy();
  }

  async onSubmit(form: NgForm) {
    const { token, tokenError } = await this.stripe.createToken(this.card);
    if (tokenError) {
    } else {
      if (!token) {
        return;
      }
      const data = {
        stripe_token: token?.id,
        npo_id: this.charityId
      };
      if (this.coupon_code) {
        // tslint:disable-next-line: no-string-literal
        data['coupon_code'] = this.coupon_code;
      }
      this.userService.makePayment(data).subscribe(response => {
        this.router.navigate(['checkout', 'thankyou']);
      }, error => {
      });
    }
  }

  onSubmitCoupon = (form: NgForm) => {
    if (form.invalid) {
      return;
    }
    this.userService.applyCoupon(form?.value).subscribe(
      (response) => {
        this.coupon_code = form?.value?.coupon_code;
        this.couponVerified = true;
        this.couponData = response?.data;
        this.couponError = null;
      },
      (error) => {
        this.coupon_code = null;
        this.couponVerified = false;
        this.couponError = error.response;
      }
    );
  }

  onClickBackToCartItems = () => {
    this.router.navigate(['checkout', 'cart-items', this.charityId]);
  }
}
