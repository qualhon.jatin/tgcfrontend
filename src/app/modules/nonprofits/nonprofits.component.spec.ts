import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonprofitsComponent } from './nonprofits.component';

describe('NonprofitsComponent', () => {
  let component: NonprofitsComponent;
  let fixture: ComponentFixture<NonprofitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonprofitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonprofitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
