import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Load Common Components
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { SharedModule } from '../../core/shared/shared.module'; // Shared Module
import { DropzoneModule, DropzoneConfigInterface, DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';

import { NonprofitsRoutingModule } from './nonprofits-routing.module';
import { NonprofitsComponent } from './nonprofits.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ProfileComponent } from './profile/profile.component'; 

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  acceptedFiles: '.jpg, .png, .jpeg, .pdf',
  createImageThumbnails: true
};


@NgModule({
  declarations: [NonprofitsComponent, LoginComponent, RegisterComponent, ForgotPasswordComponent, ResetPasswordComponent, ProfileComponent],
  imports: [
    CommonModule,
    NonprofitsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    DropzoneModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.wanderingCubes,
      backdropBackgroundColour: '#002249',
      backdropBorderRadius: '4px',
      primaryColour: '#002249',
      secondaryColour: '#002249',
      tertiaryColour: '#002249'
    }),
  ],
  providers: [
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG
    },
  ]
})
export class NonprofitsModule { }
