import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { AbstractControl, FormBuilder, FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';
import { of, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

//import Toaster Notification
import { ToastrService } from 'ngx-toastr';

//import Lodash
import * as _ from 'lodash';

// import environment
import { environment } from '../../../../environments/environment';

//import core services
import { UsersService, CommonUtilsService } from '../../../core/_services';

//import Custom Validator
import { CustomValidator } from '../../../core/helpers/custom-validator';


@UntilDestroy()
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  verifyToken: any;
  fetchUserId: any;
  tokenVerified: boolean = false;
  resetPasswordForm: FormGroup;
  resetPasswordSubmitted = false;
  public loading = false;

  constructor(private formBuilder: FormBuilder, private commonUtilsService: CommonUtilsService, private userAuthService: UsersService, private router: Router, private activatedRoute: ActivatedRoute) { }

  /**
  * Initialize Reset Password Fields.
  */
  private buildResetPasswordForm() {
    this.resetPasswordForm = this.formBuilder.group({
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50),
        // check whether the entered password has a number
        CustomValidator.patternValidator(/\d/, {
          hasNumber: true
        }),
        // check whether the entered password has upper case letter
        CustomValidator.patternValidator(/[A-Z]/, {
          hasCapitalCase: true
        }),
        // check whether the entered password has a lower case letter
        CustomValidator.patternValidator(/[a-z]/, {
          hasSmallCase: true
        }),
        // check whether the entered password has a special character
        CustomValidator.patternValidator(
          /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
          {
            hasSpecialCharacters: true
          }
        )
      ])
      ],
      confirm_password: ['', Validators.required],
      user_id: ['']
    }, {
      validator: CustomValidator.MustMatch('password', 'confirm_password')
    });
  }

  /**
   * Submit Reset Password.
   * return object array 
   */
  validateResetPasswordForm(): void {

    this.resetPasswordSubmitted = true;
    if (this.resetPasswordForm.invalid) {
      return;
    }   
    this.loading = true;
    // Update User Id in Form Control
    this.resetPasswordForm.controls.user_id.patchValue(this.fetchUserId);

    this.userAuthService.nonProfitResetPassword(this.resetPasswordForm.value).pipe(untilDestroyed(this)).subscribe(
      //case success
      (res) => {
        this.loading = false;
        this.resetPasswordSubmitted = false;
        this.commonUtilsService.onSuccess(res.response);
        this.router.navigate(['/nonprofit/login']);
        //case error 
      }, error => {
        this.loading = false;
        this.resetPasswordSubmitted = false;
        this.commonUtilsService.onError(error.response);
      });
  }


  /**
   * Verify Token Valid or not
   * return boolean 
   */
  private verifyAuthToken(verifyToken) {
    if (verifyToken) {

      this.loading = true;
      this.userAuthService.nonProfitVerifyAuthToken({ verifyToken: verifyToken }).pipe(untilDestroyed(this)).subscribe(
        //case success
        (res) => {

          this.loading = false;
          //case error 
          if (res.response) {
            this.tokenVerified = true;
            this.fetchUserId = res.user_id;
          } else {
            this.tokenVerified = false;
            this.commonUtilsService.onError(environment.MESSAGES.FAILED_TO_VERIFY);
            this.router.navigate(['/nonprofit/forgot-password']);
          }

        }, error => {
          this.loading = false;
          this.commonUtilsService.onError(error.response);
          this.tokenVerified = false;
          this.router.navigate(['/nonprofit/forgot-password']);
        });
    } else {
      this.loading = false;
      this.tokenVerified = false;
      this.commonUtilsService.onError(environment.MESSAGES.FAILED_TO_VERIFY);
      this.router.navigate(['/nonprofit/forgot-password']);
    } 
  }

  ngOnInit(): void {
    // if User Logged In then redirect to Dashboard Page
    this.userAuthService.checkLoginAndRedirect();

    this.activatedRoute.queryParams.subscribe((params) => {
      this.verifyToken = params['token'];
      this.verifyAuthToken(this.verifyToken);
    });

    this.buildResetPasswordForm(); // if verify then build reset password form
  }

  // This method must be present, even if empty.
  ngOnDestroy() {
    // To protect you, we'll throw an error if it doesn't exist.
  }


}
