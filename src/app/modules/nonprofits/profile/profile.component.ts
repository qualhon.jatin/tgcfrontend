import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { AbstractControl, FormBuilder, FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';
import { of, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
//import Lodash
import * as _ from 'lodash';

// import environment
import { environment } from '../../../../environments/environment';

//import core services
import { UsersService, CommonUtilsService } from '../../../core/_services';

//import Custom Validator
import { CustomValidator } from '../../../core/helpers/custom-validator';

// import Dropzone Module
import { DropzoneComponent, DropzoneDirective, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';

@UntilDestroy()
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  isSubmitted: boolean = false;
  userProfileForm: FormGroup;
  public loading: boolean = false;

  public npoLogoConfiguration: DropzoneConfigInterface;
  public npoProfileFileConfiguration: DropzoneConfigInterface;
  base64StringFile: any;
  npoProfileFilesArray: any = [];
  npoLogoArray: any = [];
  disabled: boolean = false;

  npoImageOrVideoPath = '';

  constructor(private formBuilder: FormBuilder, private commonUtilsService: CommonUtilsService, private userAuthService: UsersService, private router: Router, private zone: NgZone) { }

  ngOnInit(): void {
    this.initUserProfileForm(); //initilize user profile form
    this.getUserProfileData();
    this.uploadImageDropzoneInit();
  }

  private initUserProfileForm(): void {
    this.userProfileForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      organization_name: ['', Validators.required],
      name: [''],
      npo_image_or_video: this.formBuilder.array([]),
      npo_image_or_video_path: [''],
      email: ['', [Validators.email, Validators.required]],
      password: ['', Validators.compose([
        Validators.minLength(8),
        Validators.maxLength(50),
        // check whether the entered password has a number
        CustomValidator.patternValidator(/\d/, {
          hasNumber: true
        }),
        // check whether the entered password has upper case letter
        CustomValidator.patternValidator(/[A-Z]/, {
          hasCapitalCase: true
        }),
        // check whether the entered password has a lower case letter
        CustomValidator.patternValidator(/[a-z]/, {
          hasSmallCase: true
        }),
        // check whether the entered password has a special character
        CustomValidator.patternValidator(
          /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
          {
            hasSpecialCharacters: true
          }
        )
      ])
      ],
      confirm_password: [''],
    },
      {
        validator: CustomValidator.MustMatch('password', 'confirm_password')
      })
  }

  /**
   * get Profile Data
   */
  private getUserProfileData(): void {
    this.loading = true;
    this.userAuthService.getNonProfitProfile().pipe(untilDestroyed(this)).subscribe(response => {
      this.patchProfile(response);
      this.loading = false;
    }, error => {
      this.loading = false;
    })
  }

  /**
   * Update Values in All Fields
   */
  private patchProfile(response: any): void {
    let profile = response.user_details;
    this.userProfileForm.patchValue({
      first_name: profile.npo_first_name,
      last_name: profile.npo_last_name,
      email: profile.npo_email,
      organization_name: profile.organization_name,
    })
  }

  /**
   * Submit Profile Update
   */
  public submitProfile(): void {


    this.isSubmitted = true;
    if (this.userProfileForm.invalid) {
      return;
    }

    this.loading = true;

    let body = this.userProfileForm.value;
    body['name'] = this.userProfileForm.controls['first_name'].value + ' ' + this.userProfileForm.controls['last_name'].value;

    this.userAuthService.updateNonProfitProfile(body).pipe(untilDestroyed(this)).subscribe(response => {
      this.isSubmitted = false;
      this.loading = false;
      this.userAuthService.isProfileUpdated(true);
      this.commonUtilsService.onSuccess(environment.MESSAGES.PROFILE_UPDATE);
    }, error => {
      this.isSubmitted = false;
      this.loading = false;
      this.commonUtilsService.onError(error.response)
    })
  }

  /**
   * Initialize Dropzone Library(Image Upload).
   */
  private uploadImageDropzoneInit() {
    const self = this;

    this.npoProfileFileConfiguration = {
      clickable: true,
      paramName: "npo_image_or_video",
      uploadMultiple: false,
      url: environment.API_ENDPOINT + "/api/v1/nonProfit/imageUploadtoBucket",
      maxFiles: 1,
      autoReset: null,
      errorReset: null,
      cancelReset: null,
      acceptedFiles: 'image/*',
      maxFilesize: 5, // MB,
      dictDefaultMessage: '<span class="button"><i class="fa fa-pencil-square" aria-hidden="true"></i></span>',
      //previewsContainer: "#offerInHandsPreview",
      addRemoveLinks: true,
      //resizeWidth: 125,
      //resizeHeight: 125,
      //createImageThumbnails:false,
      dictInvalidFileType: 'Only valid png, jpeg, jpg, gif images are accepted.',
      dictFileTooBig: 'Maximum upload file size limit is 5MB',
      dictCancelUpload: '<i class="fa fa-times" aria-hidden="true"></i>',
      dictRemoveFile: '<i class="fa fa-times" aria-hidden="true"></i>',
      headers: {
        'Cache-Control': null,
        'X-Requested-With': null,
        'Authorization': 'Bearer '+localStorage.getItem('x-auth-token')
      },

      accept: function (file, done) {

        const reader = new FileReader();
        const _this = this
        reader.onload = function (event) {
          var base64String = reader.result
          const fileExtension = (file.name).split('.').pop();
          const isValidFile = self.commonUtilsService.isFileCorrupted(base64String, _.toLower(fileExtension))

          if (!isValidFile) {
            done('File is corrupted or invalid.');
            _this.removeFile(file);
            return false;
          }
          self.loading = true; // show page loader

          done();


        };
        reader.readAsDataURL(file);
      },
      init: function () {


        this.on('sending', function (file, xhr, formData) {
          formData.append('folder', 'npo_image_or_video');
          formData.append('fileType', file.type);
        });


        this.on("totaluploadprogress", function (progress) {
          self.loading = true;
          if (progress >= 100) {
            self.loading = false;

          }
        })

        this.on("success", function (file, serverResponse) {

          /* self.zone.run(() => {
            self.npoImagesArray.push(new FormControl({ file_path: serverResponse.fileLocation, file_name: serverResponse.fileName, file_key: serverResponse.fileKey, file_mimetype: serverResponse.fileMimeType, file_category: 'npo_image_or_video' }));
          });*/

          self.userProfileForm.patchValue({
            npo_image_or_video_path: serverResponse.fileLocation
          }) 

          self.npoImageOrVideoPath = environment.API_ENDPOINT + serverResponse.fileLocation;
          // self.updateProfilePic(serverResponse);
          this.removeFile(file);


        });

        this.on("error", function (file, error) {
          this.removeFile(file);
          self.loading = false;
          self.commonUtilsService.onError(error);
        });

      }
    };
  }


  /**
  * get Product Image Form Array
  */
  get npoImagesArray(): FormArray {
    return this.userProfileForm.controls.npo_image_or_video as FormArray;
  }


}
