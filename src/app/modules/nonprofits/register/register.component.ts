import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { AbstractControl, FormBuilder, FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';
import { of, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';


//import Toaster Notification
import { ToastrService } from 'ngx-toastr';

//import Lodash
import * as _ from 'lodash';

// import environment
import { environment } from '../../../../environments/environment';

//import core services
import { UsersService, CommonUtilsService } from '../../../core/_services';

//import Custom Validator
import { CustomValidator } from '../../../core/helpers/custom-validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  signUpForm: FormGroup;
  signUpSubmitted = false;
  public loading = false;

  constructor(private formBuilder: FormBuilder, private commonUtilsService: CommonUtilsService, private userAuthService: UsersService, private router: Router) { }

  /**
  * Initialize End User Signup Fields.
  */
  private buildSignupForm() {
    this.signUpForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50),
        // check whether the entered password has a number
        CustomValidator.patternValidator(/\d/, {
          hasNumber: true
        }),
        // check whether the entered password has upper case letter
        CustomValidator.patternValidator(/[A-Z]/, {
          hasCapitalCase: true
        }),
        // check whether the entered password has a lower case letter
        CustomValidator.patternValidator(/[a-z]/, {
          hasSmallCase: true
        }),
        // check whether the entered password has a special character
        CustomValidator.patternValidator(
          /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
          {
            hasSpecialCharacters: true
          }
        )
      ])
      ]
    });
  }

  /**
   * validate sign up form . 
   * @return json string
   */
  validateSignUpForm(): void {

    this.signUpSubmitted = true;
    if (this.signUpForm.invalid) {
      return;
    }
    this.loading = true;
    this.userAuthService.userSignUp(this.signUpForm.value).pipe(untilDestroyed(this)).subscribe(
      //case success
      (res) => {
        this.loading = false;
        this.commonUtilsService.onSuccess(res.response);
        this.router.navigate(['/user/login']);
        //case error 
      }, error => {
        this.loading = false;
        this.commonUtilsService.onError(error.response);
      });
  }

  ngOnInit(): void {
    // if User Logged In then redirect to Dashboard Page
    this.userAuthService.checkLoginAndRedirect();
    this.buildSignupForm();
  }

  // This method must be present, even if empty.
  ngOnDestroy() {
    // To protect you, we'll throw an error if it doesn't exist.
  }

}
