import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { retry, catchError } from 'rxjs/operators';
import { UsersService } from '../_services/';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private userAuthService: UsersService, private router: Router) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      retry(1),
      catchError((error: HttpErrorResponse) => {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
          // client-side error
          errorMessage = `Error: ${error.error.message}`;
        } else {
          // server-side error
          errorMessage = error.error;
        }

        let redirectUrl = '';
        if (
          errorMessage['response'] == 'Token Expired' ||
          errorMessage == 'Access denied. No token provided.'
        ) {
          if (localStorage.getItem('account_type') == 'subscriber') {
            redirectUrl = 'user';
          } else {
            redirectUrl = 'nonprofit';
          }
          localStorage.clear();
          this.userAuthService.isLoggedIn(false, '');
          this.router.navigate(['/' + redirectUrl + '/login']);
        }
        return throwError(errorMessage);
      })
    );
  }
}
