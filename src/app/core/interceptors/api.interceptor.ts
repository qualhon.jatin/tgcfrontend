import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UsersService } from '../_services/';
import { environment } from '../../../environments/environment';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    // Set Auth Token in Header
    let setHeaders;
    if (localStorage.getItem('isLoggedIn')) {
      setHeaders = { 'Authorization': 'Bearer '+localStorage.getItem('x-auth-token') };
    } else {
      setHeaders = {};
    }

    let apiReq = request.clone({ url: `${request.url}` });
   
    /* if (!(request.url).includes('moneybookers')) {      
      apiReq = request.clone({ url: environment.API_ENDPOINT + '/api/' + `${request.url}`, setHeaders: setHeaders });      
    } */

    apiReq = request.clone({ url: environment.API_ENDPOINT + '/api/v1/' + `${request.url}`, setHeaders: setHeaders }); 


    return next.handle(apiReq);
  }
}
