import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRoute,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
// modules core services
import { UsersService } from '../../core/_services';

@Injectable({
  providedIn: 'root',
})
export class UserAuthGuardService {
  returnToUrl: string;
  constructor(private userAuthService: UsersService, private router: Router) {}

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
    _activatedRoute: ActivatedRoute
  ) {
    const urlArray = route.routeConfig.path.split('/');
    urlArray[urlArray.length - 1] = route.params.id;
    this.returnToUrl = urlArray.join('/');
    if (JSON.parse(localStorage.getItem('isLoggedIn'))) {
      if (localStorage.getItem('account_type') === 'subscriber') {
        if (state.url.includes('nonprofit')) {
          localStorage.clear();
          this.userAuthService.isLoggedIn(false, '');
          this.router.navigate(['/user/login']);
        }
      }

      if (localStorage.getItem('account_type') === 'npo') {
        if (state.url.includes('user')) {
          localStorage.clear();
          this.userAuthService.isLoggedIn(false, '');
          this.router.navigate(['/nonprofit/login']);
        }
      }
      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    if (!localStorage.getItem('account_type')) {
      this.userAuthService.isLoggedIn(false, '');
      this.router.navigate(['user', 'login'], {
        queryParams: {
          returnToUrl: this.returnToUrl
        }
      });
    } else if (localStorage.getItem('account_type') === 'subscriber') {
      // localStorage.clear();
      this.userAuthService.isLoggedIn(false, '');
      this.router.navigate(['/user/login']);
    } else {
      // localStorage.clear();
      this.userAuthService.isLoggedIn(false, '');
      this.router.navigate(['nonprofit', 'login'], {
        queryParams: {
          returnToUrl: this.returnToUrl
        }
      });
    }
    return false;
  }
}
