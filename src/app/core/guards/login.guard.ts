import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRoute,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
// modules core services
import { UsersService } from '../../core/_services';

@Injectable({
  providedIn: 'root',
})
export class LoginGuardService {
  returnToUrl: string;
  constructor(private userAuthService: UsersService, private router: Router) {}

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
    _activatedRoute: ActivatedRoute
  ) {
    if (JSON.parse(localStorage.getItem('isLoggedIn'))) {
      if (localStorage.getItem('account_type') === 'subscriber') {
        this.router.navigate(['/user/profile']);
      }
      if (localStorage.getItem('account_type') === 'npo') {
        this.router.navigate(['/nonprofit/profile']);
      }
      return false;
    }
    return true;
  }
}
