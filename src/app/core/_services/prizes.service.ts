import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PrizesService {

  constructor(private httpClient: HttpClient) { }

  getPrizes(): Observable<any>{
    return this.httpClient.get('prizes');
  }
  prizeDetail(id): Observable<any>{
    return this.httpClient.get('prizeDetail/' + id);
  }
  relatedPrizes(id): Observable<any>{
    return this.httpClient.post('prizesexclude', {prize_id: id});
  }
}
