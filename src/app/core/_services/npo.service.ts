import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse,HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class NpoService {

  constructor(private httpClient: HttpClient) { }

  getNpoCategories(): Observable<any>{
    return this.httpClient.get('getNPOCategories');
  }
  getNpoList(): Observable<any>{
    return this.httpClient.get('getNPOList');
  }
  getNPOListByCategory(id): Observable<any>{
    let params = new HttpParams();
    params = params.append('category_id', id);
    return this.httpClient.get('getNPOListByCategory',{params});
  }
}
