import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public loggedIn: Subject<any> = new Subject<any>();
  public profileUpdatedStatus: Subject<any> = new Subject<any>();

  constructor(private httpClient: HttpClient, private router: Router) { }

  isLoggedIn(value: boolean, accountType: string) {
    this.loggedIn.next({ isLoggedIn: value, accountType });
  }

  checkLoggedinStatus(): Observable<any> {
    return this.loggedIn.asObservable();
  }

  isProfileUpdated(value: boolean) {
    this.profileUpdatedStatus.next(value);
  }

  getUpdatedProfileStatus(): Observable<any> {
    return this.profileUpdatedStatus.asObservable();
  }

  // if user loggedin then redirect
  checkLoginAndRedirect() {
    // checking and redirecting to user profile page
    if (this.isUserLoggedin()) {
      this.router.navigate(['/user/profile']);
    }

    // checking and redirecting to nonprofit profile page
    if (this.isNonProfitLoggedin()) {
      this.router.navigate(['/nonprofit/profile']);
    }
  }



  /** User Functions */

  isUserLoggedin(){
    // tslint:disable-next-line: triple-equals
    if (localStorage.getItem('isLoggedIn') && localStorage.getItem('account_type') == 'subscriber') {
      // logged in so return true
      return true;
    }
    return false;
  }

  // /**
  // * New User Registeration
  // * @param json
  // * @return string
  // */
  userSignUp(postedData): Observable<any> {
    return this.httpClient
      .post('user/register', postedData, { observe: 'response' })
      .map((response: HttpResponse<any>) => {
        return response;
      });
  }

  // /**
  // * User Login
  // * @return string
  // */
  userLogin(postedData): Observable<any> {
    return this.httpClient
      .post('user/login', postedData, { observe: 'response' })
      .map((response: HttpResponse<any>) => {
        return response;
      });
  }

  // /**
  // * User Logout
  // * @return object
  // */
  userLogout(postedData): Observable<any> {
    return this.httpClient
      .post('user/logout', postedData)
      .map((response: Response) => {
        return response;
      });
  }

  // /**
  // * User Forgot Password
  // * @return string
  // */
  userForgotPassword(postedData): Observable<any> {
    return this.httpClient
      .post('user/forgotPassword', postedData)
      .map((response: Response) => {
        return response;
      });
  }

  // /**
  // * User Reset Password
  // * @return string
  // */
  userResetPassword(postedData): Observable<any> {
    return this.httpClient
      .post('user/resetPassword', postedData)
      .map((response: Response) => {
        return response;
      });
  }

  // /**
  // * Verify User Auth Token
  // * @return string
  // */
  userVerifyAuthToken(postedData): Observable<any> {
    return this.httpClient
      .post('user/verifyAuthToken', postedData)
      .map((response: Response) => {
        return response;
      });
  }

  // /**
  // * get User Details
  // * @return array
  // */
  getUserProfile(): Observable<any> {
    return this.httpClient.get('user/getUserDetails');
  }
  getUserPurchases(): Observable<any> {
    return this.httpClient.get('user/getOrderHistory');
  }

  // /**
  // * update User Profile Details
  // */
  updateProfile(body: any): Observable<any> {
    return this.httpClient.post('user/updateUserProfile', body);
  }

  getCartItems(): Observable<any> {
    return this.httpClient.get('user/getCartItems');
  }
  addToCart(item: any): Observable<any> {
    return this.httpClient.post('user/addCart', item);
  }
  removeCartItem(data: any): Observable<any> {
    return this.httpClient.post('user/deleteCartItem', data);
  }
  updateCartItem(data: any): Observable<any> {
    return this.httpClient.post('user/updateCartItem', data);
  }
  applyCoupon(data: any): Observable<any> {
    return this.httpClient.get('user/applyCoupon?coupon_code=' + data.coupon_code);
  }
  makePayment(data: any): Observable<any> {
    return this.httpClient.post('user/makePayment', data);
  }
  // http://13.239.180.63/tgc/tgcproject/public/api/v1/user/makePayment
  //   Method: Post
  //   Header : Token
  //   Params : stripe_token
  /* Non Profit Functions */

  // Use LOREM25 for testing
  isNonProfitLoggedin(){
    // tslint:disable-next-line: triple-equals
    if (localStorage.getItem('isLoggedIn') && localStorage.getItem('account_type') == 'subscriber') {
      // logged in so return true
      return true;
    }
    return false;
  }


  // /**
  // * New Non Profit Registeration
  // * @param json
  // * @return string
  // */
  nonProfitSignUp(postedData): Observable<any> {
    return this.httpClient
      .post('user/register', postedData)
      .map((response: Response) => {
        return response;
      });
  }

  // /**
  // * Non Profit Login
  // * @return string
  // */
  nonProfitLogin(postedData): Observable<any> {
    return this.httpClient
      .post('nonProfit/login', postedData, { observe: 'response' })
      .map((response: HttpResponse<any>) => {
        return response;
      });
  }

  // /**
  // * Non Profit Logout
  // * @return object
  // */
  nonProfitLogout(postedData): Observable<any> {
    return this.httpClient
      .post('nonProfit/logout', postedData)
      .map((response: Response) => {
        return response;
      });
  }

  // /**
  // * User Forgot Password
  // * @return string
  // */
  nonProfitForgotPassword(postedData): Observable<any> {
    return this.httpClient
      .post('nonProfit/forgotPassword', postedData)
      .map((response: Response) => {
        return response;
      });
  }

  // /**
  // * User Reset Password
  // * @return string
  // */
  nonProfitResetPassword(postedData): Observable<any> {
    return this.httpClient
      .post('nonProfit/resetPassword', postedData)
      .map((response: Response) => {
        return response;
      });
  }

  // /**
  // * Verify User Auth Token
  // * @return string
  // */
  nonProfitVerifyAuthToken(postedData): Observable<any> {
    return this.httpClient
      .post('nonProfit/verifyAuthToken', postedData)
      .map((response: Response) => {
        return response;
      });
  }

  // /**
  // * get User Details
  // * @return array
  // */
  getNonProfitProfile(): Observable<any> {
    return this.httpClient.get('nonProfit/getNonProfitDetails');
  }

  // /**
  // * update User Profile Details
  // * @return string
  // */
  updateNonProfitProfile(body: any): Observable<any> {
    return this.httpClient.post('nonProfit/updateNonProfitProfile', body);
  }




}
