import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class CommonUtilsService {

  constructor(private httpClient: HttpClient, private toastrManager: ToastrService) { }

  /**
  * Show alert on success response & hide page loader
  * @return void
  */
  public onSuccess(message): void {
    this.toastrManager.success(message, 'Success!'); //showing success toaster 
  }

  /**
  * Show alert on error response & hide page loader
  * @return void
  */
  public onError(message): void {
    this.toastrManager.error(message, 'Oops!');//showing error toaster message  
  }

  /**
  * To check the image validity for type jpeg, png, jpg
  * @return boolean
  * @param base64string image base64 string 
  * @param type image type (jpeg, png, jpg)
  */
  public isFileCorrupted(base64string, type): boolean {

    if (type == 'png') {
      const imageData = Array.from(atob(base64string.replace('data:image/png;base64,', '')), c => c.charCodeAt(0))
      const sequence = [0, 0, 0, 0, 73, 69, 78, 68, 174, 66, 96, 130]; // in hex: 

      //check last 12 elements of array so they contains needed values
      for (let i = 12; i > 0; i--) {
        if (imageData[imageData.length - i] !== sequence[12 - i]) {
          return false;
        }
      }

      return true;
    }
    else if (type == 'pdf') {
      return true;
    }
    else if (type == 'jpeg' || type == 'jpg') {
      const imageDataJpeg = Array.from(atob(base64string.replace('data:image/jpeg;base64,', '')), c => c.charCodeAt(0))
      const imageCorrupted = ((imageDataJpeg[imageDataJpeg.length - 1] === 217) && (imageDataJpeg[imageDataJpeg.length - 2] === 255))
      return imageCorrupted;
    }
  }
}
