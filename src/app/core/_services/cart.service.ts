import { Injectable } from '@angular/core';
import { UsersService } from './users.service';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  cartItems: any[] = [];
  cartItems$ = new Subject();

  constructor(private userService: UsersService) {
    this.cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
    this.cartItems$.next([...this.cartItems]);
  }

  addToCart = (items: any[]) => {
    // tslint:disable-next-line: forin
    for (const item in items) {
      this.userService.addToCart(items[item]).subscribe(response => {
      });
    }
    localStorage.removeItem('cartItems');
  }

  getCartItems = () => {
    this.userService.getCartItems().subscribe(items => {
      if (items) {
        // tslint:disable-next-line: forin
        for (const item in this.cartItems) {
          for (const savedItem in items.data) {
            if (
              // tslint:disable-next-line: triple-equals
              this.cartItems[item].giveaway_id ==
              items.data[savedItem].giveaway_id &&
              // tslint:disable-next-line: triple-equals
              this.cartItems[item].entry_amount == items.data[savedItem].entry_amount
            ) {
              this.cartItems[item].quantity += parseInt(items.data[savedItem].quantity, 10);
              this.cartItems[item].id = items.data[savedItem].id;
              items.data.splice(parseInt(savedItem, 10), 1);
              return;
            }
          }
        }
      }
      this.cartItems = [
        ...this.cartItems,
        ...items.data,
      ];
    });
    this.cartItems$.next([...this.cartItems]);
  }
}
