import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UsersService } from '../_services/users.service';
import { PrizesService } from '../_services/prizes.service';

@Injectable()
export class MyPurchasesReolver implements Resolve<any> {
  prizeId: number;
  constructor(
    private router: Router,
    private userService: UsersService
  ) {}
  resolve = (route: ActivatedRouteSnapshot) => {
    return this.userService.getUserPurchases();
  }
}
