import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UsersService } from '../_services/users.service';
import { PrizesService } from '../_services/prizes.service';

@Injectable()
export class GiveawayDetailsReolver implements Resolve<any> {
  prizeId: number;
  constructor(
    private userService: UsersService,
    private router: Router,
    private prizeService: PrizesService
  ) {}
  resolve = (route: ActivatedRouteSnapshot) => {
    this.prizeId = route.params.id;
    return this.prizeService.prizeDetail(this.prizeId);
  }
}
