import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UsersService } from '../_services/users.service';

@Injectable()

export class CartItemsReolver implements Resolve<any> {
    constructor(private userService: UsersService, private router: Router) {}

    resolve = (route: ActivatedRouteSnapshot): Observable<any> => {
        if (localStorage.getItem('isLoggedIn')) {
            this.userService.getCartItems().subscribe(items => {
                if (items?.data.length === 0) {
                    this.router.navigate(['checkout', 'cart-items', route.params.id]);
                }
            });
            return this.userService.getCartItems();
        }
    }
}