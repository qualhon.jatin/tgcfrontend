import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';

import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

//import core services
import { UsersService, CommonUtilsService } from '../../../core/_services';

@UntilDestroy()
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLoggedin: boolean = false;

  loginSubscription: Subscription;

  constructor(private router: Router, private userAuthService: UsersService, private commonUtilsService: CommonUtilsService) { }


  /*
    Logout User
  */
  public logout() {
    this.userAuthService.userLogout({ auth_token: localStorage.getItem('x-auth-token') }).pipe(untilDestroyed(this)).subscribe(
      //case success
      (res) => {
        this.commonUtilsService.onSuccess(res.response);
        let redirectUrl = '';
        if (localStorage.getItem('account_type') == 'subscriber') {
          redirectUrl = 'user'
        } else {
          redirectUrl = 'nonprofit'
        }

        localStorage.removeItem('x-auth-token');
        localStorage.removeItem('account_type');
        localStorage.removeItem('isLoggedIn');
        localStorage.clear();
        this.userAuthService.isLoggedIn(false, ''); //trigger loggedin observable 
        this.router.navigate(['/' + redirectUrl + '/login']);
        //case error 
      }, error => {
        this.commonUtilsService.onError(error.response);
      });
  }

  ngOnInit(): void {
    // Page Refresh
    if (localStorage.getItem('isLoggedIn')) {
      this.isLoggedin = true;
    }

    this.loginSubscription = this.userAuthService.checkLoggedinStatus().subscribe((loginStatus) => {
      this.isLoggedin = loginStatus.isLoggedIn;
    });


  }

  // This method must be present, even if empty.
  ngOnDestroy() {
    // To protect you, we'll throw an error if it doesn't exist.
  }

}
