import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule, HttpClient } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/components/header/header.component';
import { FooterComponent } from './core/components/footer/footer.component';



//importing intercepters
import { ApiInterceptor } from './core/interceptors/api.interceptor';
import { HttpErrorInterceptor } from './core/interceptors/http-error.interceptor';

//import core services
import { UsersService, CommonUtilsService,PrizesService } from './core/_services';

//import shared module
import { SharedModule } from './core/shared/shared.module';

import { HomeComponent } from './modules/homepages/home/home.component';
import { BannerComponent } from './modules/homepages/home/banner/banner.component';
import { PrizeSectionComponent } from './modules/homepages/home/prize-section/prize-section.component';
import { ExploreSectionComponent } from './modules/homepages/home/explore-section/explore-section.component';
import { HelpSectionComponent } from './modules/homepages/home/help-section/help-section.component';
import { FeedbackSectionComponent } from './modules/homepages/home/feedback-section/feedback-section.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { ContactComponent } from './modules/homepages/contact/contact.component';
import { ChampionsComponent } from './modules/homepages/champions/champions.component';
import { GiveawaysComponent } from './modules/homepages/giveaways/giveaways.component';

import {MatTabsModule} from '@angular/material/tabs';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import { FaqComponent } from './modules/homepages/faq/faq.component';

import { MatExpansionModule } from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import { GiveawayDetailsComponent } from './modules/homepages/giveaway-details/giveaway-details.component';
import { GiveawayDetailsBannerComponent } from './modules/homepages/giveaway-details/giveaway-details-banner/giveaway-details-banner.component';
import { GiveawayDetailsDescriptionComponent } from './modules/homepages/giveaway-details/giveaway-details-description/giveaway-details-description.component';
import { GiveawayDetailsEntriesComponent } from './modules/homepages/giveaway-details/giveaway-details-entries/giveaway-details-entries.component';
import { GiveawayDetailsMorePrizesComponent } from './modules/homepages/giveaway-details/giveaway-details-more-prizes/giveaway-details-more-prizes.component'
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule } from '@angular/forms';
import { CountdownModule } from 'ngx-countdown';
import { CategoryListComponent } from './modules/checkout/category-list/category-list.component';
import { CharityListComponent } from './modules/checkout/charity-list/charity-list.component';
import { CartItemsComponent } from './modules/checkout/cart-items/cart-items.component';
import { PaymentDetailComponent } from './modules/checkout/payment-detail/payment-detail.component';
import { ThankyouComponent } from './modules/checkout/thankyou/thankyou.component';
import { OnlyNumberDirective } from './core/custom-directives/only-number.directive';
import { CartItemsReolver } from './core/resolvers/cart-items.resolver';
import { LoginGuardService } from './core/guards/login.guard';
import { GiveawayDetailsReolver } from './core/resolvers/giveaway-detail.resolver';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    BannerComponent,
    PrizeSectionComponent,
    ExploreSectionComponent,
    HelpSectionComponent,
    FeedbackSectionComponent,
    ContactComponent,
    ChampionsComponent,
    GiveawaysComponent,
    FaqComponent,
    GiveawayDetailsComponent,
    GiveawayDetailsBannerComponent,
    GiveawayDetailsDescriptionComponent,
    GiveawayDetailsEntriesComponent,
    GiveawayDetailsMorePrizesComponent,
    CategoryListComponent,
    CharityListComponent,
    CartItemsComponent,
    PaymentDetailComponent,
    ThankyouComponent,
    OnlyNumberDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule,
    CarouselModule,
    MatTabsModule,
    MatButtonModule,
    MatInputModule,
    MatExpansionModule,
    MatIconModule,
    Ng2SearchPipeModule,
    FormsModule,
    CountdownModule
  ],
  providers: [
    UsersService,
    PrizesService,
    CommonUtilsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor, multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor, multi: true
    },
    CartItemsReolver,
    GiveawayDetailsReolver,
    LoginGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
