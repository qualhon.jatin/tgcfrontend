import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './modules/homepages/home/home.component';
import { ContactComponent } from './modules/homepages/contact/contact.component';
import { ChampionsComponent } from './modules/homepages/champions/champions.component';
import { GiveawaysComponent } from './modules/homepages/giveaways/giveaways.component';
import { FaqComponent } from './modules/homepages/faq/faq.component';
import { GiveawayDetailsComponent } from './modules/homepages/giveaway-details/giveaway-details.component';
import { CategoryListComponent } from './modules/checkout/category-list/category-list.component';
import { CharityListComponent } from './modules/checkout/charity-list/charity-list.component';
import { CartItemsComponent } from './modules/checkout/cart-items/cart-items.component';
import { PaymentDetailComponent } from './modules/checkout/payment-detail/payment-detail.component';
import { ThankyouComponent } from './modules/checkout/thankyou/thankyou.component';
import { UserAuthGuardService } from './core/guards/user-auth-guard.service';
import { CartItemsReolver } from './core/resolvers/cart-items.resolver';
import { GiveawayDetailsReolver } from './core/resolvers/giveaway-detail.resolver';

const routes: Routes = [
  { path: 'user', loadChildren: () => import('./modules/users/users.module').then(m => m.UsersModule) },
  { path: 'nonprofit', loadChildren: () => import('./modules/nonprofits/nonprofits.module').then(m => m.NonprofitsModule) },
  {
    path: '',
    component: HomeComponent,
    data: { title: 'Home' }
  },
  {
    path: 'contact',
    component: ContactComponent,
    data: { title: 'Contact' }
  },
  {
    path: 'champions',
    component: ChampionsComponent,
    data: { title: 'Champions' }
  },
  {
    path: 'giveaways',
    component: GiveawaysComponent,
    data: { title: 'Giveaways' }
  },
  {
    path: 'faq',
    component: FaqComponent,
    data: { title: 'Faq' }
  },
  {
    path: 'giveaway-details/:id',
    component: GiveawayDetailsComponent,
    data: { title: 'Giveaway' },
    runGuardsAndResolvers: 'always',
    resolve: {
      giveawayDetail: GiveawayDetailsReolver
    }
  },
  {
    path: 'checkout/category-list',
    component: CategoryListComponent,
    data: { title: 'Categories' }
  },
  {
    path: 'checkout/charity-list/:id',
    component: CharityListComponent,
    data: { title: 'Charity' }
  },
  {
    path: 'checkout/cart-items/:id',
    component: CartItemsComponent,
    resolve: {
      cartItems: CartItemsReolver
    },
    data: { title: 'Cart' }
  },
  {
    path: 'checkout/payment-detail/:id',
    component: PaymentDetailComponent,
    canActivate: [UserAuthGuardService],
    runGuardsAndResolvers: 'always',
    resolve: {
      cartItems: CartItemsReolver
    },
    data: { title: 'Payment detail' }
  },
  {
    path: 'checkout/thankyou',
    component: ThankyouComponent,
    data: { title: 'Thankyou' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: false, useHash: true, scrollPositionRestoration: 'top'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
