export const environment = {
  production: true,
  API_ENDPOINT: 'http://13.239.180.63/tgc/tgcproject/public',
  WEB_ENDPOINT: 'http://localhost:4200',
  MESSAGES: {

    ERROR_TEXT_LOADER: 'Oops! Something went nuts',

    SIGNING_UP: 'Signing up... please wait',

    WAIT_TEXT: 'Please wait...',

    VALIDATION_ERROR: 'There’s something wrong on your details',

    UNABLE_TO_FIND_DETAILS: 'Unable to find details.',

    PAYMENT_FAILED: 'Sorry, we could not process your payment.',

    FAILED_TO_VERIFY: "Your token has expired. Please try logging back again.",

    CANNOT_UPLOAD_MORE: "You cannot upload any more files.",

    PROFILE_UPDATE: 'Profile updated successfully.',

    PAYENT_SUCCESS: 'Payment Done',

    CREDIT_CARD_INVALID: 'You have wrong details',

    PROFILE_IMAGE_UPDATE: 'Profile image updated',

    PASSWORD_CHANGE: 'Password has been changed'

  }
};
